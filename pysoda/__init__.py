# coding: utf-8

"""
Pysoda (Python Service-Oriented Dataset Abstraction) API.
"""

from .src.datasets import *
from .src.datastores import *
from .src.storage_backends import *
load_datasets()


# ========== #
# API object #
# ========== #

class PysodaAPI(object):
    """
    API object.
    :version: 0.1
    :author: Dimitri Justeau
    """
    def __init__(self, storage_backend):
        self._storage_backend = storage_backend

    @property
    def storage_backend(self):
        return self._storage_backend

    @property
    def datastores(self):
        return self.storage_backend.get_datastores()
