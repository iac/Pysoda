# coding: utf-8

from ...datastores.datastore_base import *
from .datastore import SQLDatastore


class SQLDatastoreJsonAdapter(DatastoreStorageAdapter):
    """
    SQL datastore adapter for json storage backend.
    """
    def register_datastore(self, datastore):
        self.storage_backend._register(datastore,
                                       {'url': datastore.url})

    def load_datastore(self, datastore_name):
        kwargs = self.storage_backend.load_datastore(datastore_name)
        return SQLDatastore(datastore_name, kwargs["kwargs"]["url"])


class SQLDatastoreDjangoAdapter(SQLDatastoreJsonAdapter):
    pass
