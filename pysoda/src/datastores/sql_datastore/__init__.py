# coding: utf-8

from ..datastore_base import DatastoreFactory, DATASTORE_FACTORIES
from ...storage_backends.json_storage_backend import JsonStorageBackend
from ...storage_backends.django_storage_backend import DjangoStorageBackend
from .datastore import SQLDatastore
from .adapters import SQLDatastoreJsonAdapter, SQLDatastoreDjangoAdapter


ADAPTERS = {
    JsonStorageBackend: SQLDatastoreJsonAdapter,
    DjangoStorageBackend: SQLDatastoreDjangoAdapter,
}


class SQLDatastoreFactory(DatastoreFactory):
    @classmethod
    def get_adapter(cls, storage_backend):
        return ADAPTERS[storage_backend.__class__](storage_backend)


DATASTORE_FACTORIES[SQLDatastore] = SQLDatastoreFactory
cls_name = SQLDatastore.__name__
JsonStorageBackend.SUPPORTED_DATASTORES[cls_name] = SQLDatastore
DjangoStorageBackend.SUPPORTED_DATASTORES[cls_name] = SQLDatastore
