# coding: utf-8

from contextlib import contextmanager
import json

from sqlalchemy import create_engine, Table, MetaData, Column, Integer, String
from sqlalchemy.sql import select

from ..datastore_base import *


class SQLDatastore(Datastore):
    """
    Datastore for storing datasets in relational database tables. The datasets
    metadata is stored in a special table, called "datasets_meta".
    Relying on SQLAlchemy's Core, this datastore is database-agnostic.
    """

    SUPPORTED_DATASETS = {}

    DATASETS_META_TABLE_NAME = "datasets_meta"

    def __init__(self, name, url, init=False):
        """
        :param name: The name of the datastore.
        :param url: The database url, same as defined in SQLAlchemy:
                    dialect+driver://username:password@host:port/database
        """
        super(SQLDatastore, self).__init__(name)
        self._url = url
        self._metadata = MetaData()
        self._datasets_meta = Table(
            self.DATASETS_META_TABLE_NAME, self._metadata,
            Column('id', Integer, primary_key=True),
            Column('name', String, unique=True, nullable=False),
            Column('cls_name', String, nullable=False),
            Column('kwargs', String),
        )
        if init:
            self._init()

    def _init(self):
        """
        Initialize the "datasets_meta" table.
        """
        engine = self._get_engine()
        self._metadata.create_all(engine)

    @property
    def url(self):
        return self._url

    @contextmanager
    def _get_connection(self):
        conn = None
        try:
            conn = self._get_engine().connect()
            yield conn
        except:
            raise
        finally:
            if conn:
                conn.close()

    def get_dataset_key(self, dataset_name):
        s = select([self._datasets_meta, ]) \
            .where(self._datasets_meta.c.name == dataset_name)
        with self._get_connection() as conn:
            results = conn.execute(s)
            row = results.fetchone()
            ds_name, cls_name = row[1], row[2]
            ds_cls = self.SUPPORTED_DATASETS[cls_name]
            return ds_name, ds_cls

    def get_dataset_keys(self):
        keys = list()
        s = select([self._datasets_meta, ])
        with self._get_connection() as conn:
            results = conn.execute(s)
            for row in results:
                ds_name, cls_name = row[1], row[2]
                ds_cls = self.SUPPORTED_DATASETS[cls_name]
                keys.append((ds_name, ds_cls))
            return keys

    def register_(self, dataset, kwargs):
        ins = self._datasets_meta.insert()
        with self._get_connection() as conn:
            conn.execute(
                ins,
                name=dataset.name,
                cls_name=dataset.__class__.__name__,
                kwargs=json.dumps(kwargs),
            )

    def register_existing_table(self, table_name, dataset_cls, kwargs):
        ins = self._datasets_meta.insert()
        with self._get_connection() as conn:
            conn.execute(
                ins,
                name=table_name,
                cls_name=dataset_cls,
                kwargs=json.dumps(kwargs),
            )

    def ls_existing_tables(self):
        engine = self._get_engine()
        metadata = MetaData()
        metadata.reflect(engine)
        return metadata.tables.keys()

    def create_table(self, table_name, columns):
        args = [table_name, self._metadata] + columns
        Table(*args)
        engine = self._get_engine()
        self._metadata.create_all(engine)

    def get_table(self, table_name):
        """
        :param table_name: The of the table to reflect.
        :return: The table object corresponding to the reflected table.
        """
        return Table(table_name, self._metadata, autoload=True,
                     autoload_with=self._get_engine())

    def get_table_rows(self, table_name, exclude=[], filters={}):
        """
        :param table_name: The table to query.
        :return: All the rows of table.
        """
        table = self.get_table(table_name)
        select_cols = list()
        for col in table.columns:
            if col.name not in exclude:
                select_cols.append(col)
        s = select(select_cols)
        if len(filters) > 0:
            where_clauses = list()
            for c, v in filters.items():
                where_clauses.append(getattr(table.c, c) == v)
            s = s.where(*where_clauses)
        with self._get_connection() as conn:
            results = conn.execute(s)
            return results.fetchall()

    def insert_row(self, table_name, row):
        ins = self.get_table(table_name).insert().values(row)
        with self._get_connection() as conn:
            conn.execute(ins)

    def _get_engine(self):
        return create_engine(self.url)

    def supported_datasets(self):
        return self.SUPPORTED_DATASETS.values()
