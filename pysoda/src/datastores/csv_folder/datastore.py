# coding: utf-8

import os
import json
import csv

import numpy as np

from pysoda.src.datastores.datastore_base import *


PROPERTIES_FILE_NAME = "properties.json"


class CsvFolderDatastore(Datastore):
    """
    Implementation of the datastore where datasets are csv files, stored in
    the same folder.
    """

    SUPPORTED_DATASETS = {}

    def __init__(self, name, path, init=False):
        super(CsvFolderDatastore, self).__init__(name)
        self._path = path
        if init:
            self._init()

    @property
    def path(self):
        return self._path

    @property
    def properties_path(self):
        return os.path.join(self.path, PROPERTIES_FILE_NAME)

    def get_dataset_keys(self):
        datasets = self._load_properties()["datasets"]
        keys = list()
        for ds_name, ds_props in datasets.items():
            ds_cls = self.SUPPORTED_DATASETS[ds_props["cls_name"]]
            keys.append((ds_name, ds_cls))
        return keys

    def get_dataset_key(self, dataset_name):
        datasets = self._load_properties()["datasets"]
        props = datasets[dataset_name]
        return dataset_name, self.SUPPORTED_DATASETS[props["cls_name"]]

    def get_dataset_kwargs(self, dataset_name):
        return self._load_properties()["datasets"][dataset_name]["kwargs"]

    def set_dataset_kwargs(self, dataset_name, kwargs):
        properties = self._load_properties()
        properties["datasets"][dataset_name]["kwargs"] = kwargs
        self._write_properties(properties)

    def load_csv_file(self, filename, exclude=[], filters={},
                      has_header=False):
        """
        Load a csv file.
        :param filename: The filename to load from (relative to the csv
                         folder path).
        :param exclude: The columns to exclude.
        :param filters: Filters to apply on columns.
            Dict with columns <=> value to retain.
        :param has_header: True if the first row is the header.
        :return: (header - None if has_header is false, rows).
        """
        with open(os.path.join(self.path, filename), 'r') as csv_file:
            reader = csv.reader(csv_file)
            header = list()
            rows = [tuple(row) for row in reader]
            if has_header:
                header = rows.pop(0)
            if len(exclude) > 0:
                exclude_idx = list()
                for i, h in enumerate(header):
                    # Typed table case TODO: Make it less hackish
                    if isinstance(h, (list, tuple)):
                        if h[0] in exclude:
                            exclude_idx.append(i)
                    else:
                        if h in exclude:
                            exclude_idx.append(i)
                array = np.array(rows)
                rows = list(map(tuple, np.delete(array, exclude_idx, 1)))
            return header, rows

    def write_csv_file(self, filename, rows, header=None):
        """
        Write to a csv file.
        :param filename: The filename to write to (relative to the csv
                         folder path).
        :param header: The header, optional.
        :param rows: The data rows.
        """
        with open(os.path.join(self.path, filename), 'w') as csv_file:
            writer = csv.writer(csv_file)
            if header is not None:
                writer.writerow(header)
            writer.writerows(rows)

    def _init(self):
        """
        Init the properties json file.
        """
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        properties_json = {
            "datasets": {},
        }
        self._write_properties(properties_json)

    def _register(self, dataset, dataset_kwargs):
        properties = self._load_properties()
        properties["datasets"][dataset.name] = {
            "kwargs": dataset_kwargs,
            "cls_name": dataset.__class__.__name__
        }
        self._write_properties(properties)

    def _load_properties(self):
        with open(self.properties_path, 'r') as props:
            json_str = props.read()
            return json.loads(json_str)

    def _write_properties(self, properties):
        with open(self.properties_path, 'w') as props:
            json.dump(properties, props)

    def supported_datasets(self):
        return self.SUPPORTED_DATASETS.values()
