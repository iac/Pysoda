# coding: utf-8

from ...datastores.datastore_base import *
from .datastore import CsvFolderDatastore


class CsvFolderJsonAdapter(DatastoreStorageAdapter):
    """
    Csv folder storage adapter for json storage backend.
    """
    def register_datastore(self, datastore):
        self.storage_backend._register(datastore,
                                       {'path': datastore.path})

    def load_datastore(self, datastore_name):
        kwargs = self.storage_backend.load_datastore(datastore_name)
        return CsvFolderDatastore(datastore_name, kwargs["kwargs"]["path"])


class CsvFolderDjangoAdapter(CsvFolderJsonAdapter):
    pass
