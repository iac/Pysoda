# coding: utf-8

from ..datastore_base import DatastoreFactory, DATASTORE_FACTORIES
from ...storage_backends.json_storage_backend import JsonStorageBackend
from ...storage_backends.django_storage_backend import DjangoStorageBackend
from .datastore import CsvFolderDatastore
from .adapters import CsvFolderJsonAdapter, CsvFolderDjangoAdapter


ADAPTERS = {
    JsonStorageBackend: CsvFolderJsonAdapter,
    DjangoStorageBackend: CsvFolderDjangoAdapter,
}


class CsvFolderFactory(DatastoreFactory):
    @classmethod
    def get_adapter(cls, storage_backend):
        return ADAPTERS[storage_backend.__class__](storage_backend)


DATASTORE_FACTORIES[CsvFolderDatastore] = CsvFolderFactory
cls_name = CsvFolderDatastore.__name__
JsonStorageBackend.SUPPORTED_DATASTORES[cls_name] = CsvFolderDatastore
DjangoStorageBackend.SUPPORTED_DATASTORES[cls_name] = CsvFolderDatastore
