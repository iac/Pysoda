# coding: utf-8

from ...datastores.datastore_base import *
from .datastore import MockDatastore


class MockJsonAdapter(DatastoreStorageAdapter):
    """
    Csv folder storage adapter for simple json storage backend.
    """
    def register_datastore(self, datastore):
        self.storage_backend._register(datastore, {'name': datastore.name})

    def load_datastore(self, datastore_name):
        return MockDatastore(datastore_name)


class MockDjangoAdapter(MockJsonAdapter):
    pass
