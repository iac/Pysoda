# coding: utf-8

from pysoda.src.datastores.datastore_base import *


class MockDatastore(Datastore):

    SUPPORTED_DATASETS = {}

    def __init__(self, name):
        super(MockDatastore, self).__init__(name)
        self.datasets = list()
        self.datasets_contents = dict()

    def get_dataset_key(self, dataset_name):
        # Very bad
        for dataset in self.datasets:
            if dataset.name == dataset_name:
                return dataset.name, dataset.__class__

    def get_dataset_keys(self):
        return [(d.name, d.__class__) for d in self.datasets]

    def supported_datasets(self):
        return self.SUPPORTED_DATASETS.values()
