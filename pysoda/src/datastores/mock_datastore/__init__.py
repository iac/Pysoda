# coding: utf-8

from ..datastore_base import DatastoreFactory, DATASTORE_FACTORIES
from ...storage_backends.json_storage_backend import JsonStorageBackend
from ...storage_backends.django_storage_backend import DjangoStorageBackend
from .datastore import MockDatastore
from .adapters import MockJsonAdapter, MockDjangoAdapter


ADAPTERS = {
    JsonStorageBackend: MockJsonAdapter,
    DjangoStorageBackend: MockDjangoAdapter,
}


class MockDatastoreFactory(DatastoreFactory):
    @classmethod
    def get_adapter(cls, storage_backend):
        return ADAPTERS[storage_backend.__class__](storage_backend)


DATASTORE_FACTORIES[MockDatastore] = MockDatastoreFactory
cls_name = MockDatastore.__name__
JsonStorageBackend.SUPPORTED_DATASTORES[cls_name] = MockDatastore
DjangoStorageBackend.SUPPORTED_DATASTORES[cls_name] = MockDatastore
