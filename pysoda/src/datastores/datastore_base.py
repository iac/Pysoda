# coding: utf-8

import abc

from ..datasets.dataset_base import DATASET_FACTORIES


DATASTORE_FACTORIES = {}


class DatastoreFactory(abc.ABC):
    """
    Abstract factory for datastores.
    """
    @classmethod
    def load_datastore(cls, datastore_name, storage_backend):
        """
        Make a datastore.
        """
        return cls.get_adapter(storage_backend).load_datastore(datastore_name)

    @classmethod
    @abc.abstractmethod
    def get_adapter(cls, storage_backend):
        """
        Return the corresponding adapter for the given storage backend.
        """


class Datastore(abc.ABC):
    """
    Abstract base class for representing a datastore.
    """
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    @abc.abstractmethod
    def supported_datasets(self):
        """
        :return: A list of the supported dataset classes.
        """

    @abc.abstractmethod
    def get_dataset_keys(self):
        """
        :return: The registered dataset keys, ie (dataset_name, dataset_cls).
        """

    @abc.abstractmethod
    def get_dataset_key(self, dataset_name):
        """
        :param dataset_name: The dataset name to find.
        :return: The dataset key (dataset_name, dataset_cls).
        """

    def get_datasets(self):
        """
        :return The datasets available within this datastore.
        """
        datasets = list()
        for dataset_name, dataset_cls in self.get_dataset_keys():
            factory = DATASET_FACTORIES[dataset_cls]
            dataset = factory.load_dataset(dataset_name, self)
            datasets.append(dataset)
        return datasets

    def get_dataset(self, dataset_name):
        """
        :param dataset_name: The dataset name to get.
        :return: The dataset within this datastore having this name.
        """
        dataset_cls = self.get_dataset_key(dataset_name)[1]
        factory = DATASET_FACTORIES[dataset_cls]
        dataset = factory.load_dataset(dataset_name, self)
        return dataset

    def register_dataset(self, dataset):
        """
        Register a dataset to the datastore.
        :param dataset: The dataset to register.
        """
        factory = DATASET_FACTORIES[dataset.__class__]
        dialect = factory.get_dialect(self)
        dialect.register_dataset(dataset)


class DatastoreStorageAdapter(abc.ABC):
    """
    Abstract base class for datastore storage adapter.
    """
    def __init__(self, storage_backend):
        self._storage_backend = storage_backend

    @property
    def storage_backend(self):
        return self._storage_backend

    @abc.abstractmethod
    def load_datastore(self, datastore_name):
        """
        :param datastore_name: The name of the datastore to load.
        :return: The loaded datastore.
        """

    @abc.abstractmethod
    def register_datastore(self, datastore):
        """
        :param datastore: The datastore to register.
        """
