# coding: utf-8

import abc

import geojson
from shapely.geometry import mapping

from ..table_service import TableReadService


class GeoTableService(TableReadService):
    """
    Geographic extension to table read services.
    """

    def get_export_formats(self):
        return TableReadService.get_export_formats(self) + ['geojson', ]

    def rows_to_geojson(self, rows, geometry_column=None):
        if geometry_column is None:
            geometry_column = self.find_geometry_column()
        geometry_column_index = self.get_column_index(geometry_column)
        features = list()
        for row in rows:
            indexes = list(range(len(self.get_header())))
            indexes.pop(geometry_column_index)
            headers_name = self.get_header_names()
            properties = {headers_name[i]: str(row[i]) for i in indexes}
            dialect_geom = row[geometry_column_index]
            geom = mapping(self.to_shape(dialect_geom))
            feature = geojson.Feature(geometry=geom, properties=properties)
            features.append(feature)
        feature_collection = geojson.FeatureCollection(
            features,
        )

        return geojson.dumps(feature_collection)

    @abc.abstractmethod
    def to_shape(self, geometry_object):
        """
        :param geometry_object: The geometry object
        :return: A shapely shape from a dialect specific geometry object.
        """

    def _get_export_methods(self):
        base = super(GeoTableService, self)._get_export_methods()
        base.update({
            'geojson': self._export_to_geojson,
        })
        return base

    def _export_to_geojson(self, file, geometry_column=None):
        if geometry_column is None:
            geometry_column = self.find_geometry_column()
        geometry_column_index = self.get_column_index(geometry_column)
        features = list()
        for row in self.get_rows():
            indexes = list(range(len(self.get_header())))
            indexes.pop(geometry_column_index)
            headers_name = self.get_header_names()
            properties = {headers_name[i]: str(row[i]) for i in indexes}
            dialect_geom = row[geometry_column_index]
            geom = mapping(self.to_shape(dialect_geom))
            feature = geojson.Feature(geometry=geom, properties=properties)
            features.append(feature)
        feature_collection = geojson.FeatureCollection(features)
        geojson.dump(feature_collection, file)
        return file
