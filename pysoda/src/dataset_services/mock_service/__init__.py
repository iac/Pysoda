# coding: utf-8

import abc

from ..dataset_service_base import DatasetService


class MockService(DatasetService):
    """
    Mock service for testing.
    """
    @classmethod
    def name(cls):
        return "Mock service"

    @classmethod
    def description(cls):
        return "This is a mock service for testing issues. It enables " \
            + "different animal noises to have fun with datasets."

    @abc.abstractmethod
    def bark(self):
        """
        Bark like a dog.
        """

    @abc.abstractmethod
    def meow(self):
        """
        Meow like a cat.
        """

    @abc.abstractmethod
    def cocorico(self):
        """
        Cocorico like a cocorico.
        """
