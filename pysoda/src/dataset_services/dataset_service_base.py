# coding: utf-8

import abc


class DatasetService(abc.ABC):
    """
    Abstract base class for defining a service provided by a dataset.
    Every subclass of service DEFINING a service should be abstract, ie pure
    service objects should never be instantiated.
    To provide a service, a dataset must extend this service, and thus
    implement the abstract methods defined by this service.
    """
    @classmethod
    @abc.abstractmethod
    def name(cls):
        """
        :return: The service name.
        """

    @classmethod
    @abc.abstractmethod
    def description(cls):
        """
        :return: The service description.
        """
