# coding: utf-8

import abc

from ..dataset_service_base import DatasetService


class TableReadService(DatasetService):
    """
    Services provided by a simple read-only row-oriented table dataset.
    """

    @classmethod
    def name(cls):
        return "Table service"

    @classmethod
    def description(cls):
        return "Simple read row-oriented table service."

    @abc.abstractmethod
    def get_header(self, exclude=[]):
        """
        :param exclude: The columns to exclude.
        :return: The column headers of the table in a list of str.
        """

    def get_header_names(self, exclude=[]):
        """
        :param exclude: The columns to exclude.
        :return: The column headers, just the names (for typed tables)
        """
        return self.get_header(exclude=exclude)

    def get_column_index(self, column_name):
        """
        :param column_name: The column's name.
        :return: The index of the the column according to the
                 given column name.
        """
        headers = self.get_header_names()
        for i, h in enumerate(headers):
            if h == column_name:
                return i
        raise ValueError("There is no such column: '{}'".format(column_name))

    @abc.abstractmethod
    def get_rows(self, exclude=[], filters={}):
        """
        :param exclude: The columns to exclude.
        :param filters: Filters to apply on columns.
            Dict with columns <=> value to retain.
        :return: The rows of the table.
        """

    def get_export_formats(self):
        return ['csv', ]

    def _get_export_methods(self):
        return {
            'csv': self._export_to_csv,
        }

    def export(self, export_format, file, **kwargs):
        """
        Export the dataset according to the requested format.
        Take a file handler to write the data.
        Supported formats are:
            - 'csv'
        """
        if export_format not in self.get_export_formats():
            raise ValueError(
                "The export format '{}' is not supported. Supported are: {}."
                .format(export_format, ','.join(self.get_export_formats())))
        return self._get_export_methods()[export_format](file, **kwargs)

    def _export_to_csv(self, file):
        import csv
        header = self.get_header()
        data = self.get_rows()
        writer = csv.writer(file)
        writer.writerow(header)
        writer.writerows(data)
        return file


class TableWriteService(DatasetService):
    """
    Services provided by a simple write-only row-oriented table dataset.
    """

    @classmethod
    def name(cls):
        return "Table service"

    @classmethod
    def description(cls):
        return "Simple write row-oriented table service."

    @abc.abstractmethod
    def append_row(self, row):
        """
        Append a row to the end of the table.
        :param row: The row to append. Must be the same length as the header.
        """

    @abc.abstractmethod
    def append_rows(self, rows):
        """
        Appends a list of rows to the end of the table.
        A basic implementation based on the append_row method is provided,
        but it in most cases it is probably a better strategy in term of
        performances to overwrite it according to the underlying datastore
        backend.
        :param rows: The rows to append.
        """
        for row in rows:
            self.append_row(row)
