# coding: utf-8

import abc
import logging


from ..datastores.datastore_base import DATASTORE_FACTORIES


LOGGER = logging.getLogger()


DATASTORE_STORAGE_ADAPTERS = {}


class Meta(abc.ABCMeta):
    def __init__(cls, name, bases, dic):
        abc.ABCMeta.__init__(cls, name, bases, dic)
        DATASTORE_STORAGE_ADAPTERS[cls] = {}


class StorageBackend(metaclass=Meta):
    """
    Abstract base class for internal storage backend.
    """
    @abc.abstractmethod
    def get_datastore_keys(self):
        """
        :return: The registered datastore keys.
        ie (datastore_name, datastore_cls)
        """

    @abc.abstractmethod
    def get_datastore_key(self, datastore_name):
        """
        :param datastore_name: The datastore name to find.
        :return: The datastore key (datastore_name, datastore_cls).
        """

    def get_datastores(self):
        """
        :return: The registered datastores.
        """
        datastores = list()
        for datastore_name, datastore_cls in self.get_datastore_keys():
            factory = DATASTORE_FACTORIES[datastore_cls]
            datastore = factory.load_datastore(datastore_name, self)
            datastores.append(datastore)
        return datastores

    def get_datastore(self, datastore_name):
        """
        :param datastore_name: The datastore name to get.
        :return: The datastore within this storage backend having this name.
        """
        datastore_cls = self.get_datastore_key(datastore_name)[1]
        factory = DATASTORE_FACTORIES[datastore_cls]
        datastore = factory.load_datastore(datastore_name, self)
        return datastore

    def register_datastore(self, datastore):
        """
        Register a datastore within this storage backend.
        :param datastore: The datastore to register.
        """
        factory = DATASTORE_FACTORIES[datastore.__class__]
        adapter = factory.get_adapter(self)
        adapter.register_datastore(datastore)

    @classmethod
    def register_datastore_storage_adapter(cls, datastore_cls,
                                           storage_adapter_cls):
        """
        For a given datastore class, register the datastore storage adapter
        class that must be used.
        :param datastore_cls: The Datastore class
        :param storage_adapter_cls: The Datastore storage adapter class
        """
        v = DATASTORE_STORAGE_ADAPTERS[cls]
        if datastore_cls in v:
            LOGGER.warning("{} was already defined as the storage adapter "
                           + "for {}. Replacing it with {}."
                           .format(v[datastore_cls], datastore_cls,
                                   storage_adapter_cls))
        v[datastore_cls] = storage_adapter_cls
