# coding: utf-8

import json
import os

from .storage_backend_base import StorageBackend


class JsonStorageBackend(StorageBackend):
    """
    Simple implementation of the storage backend where all the datastores
    are stored within a json file.
    """

    SUPPORTED_DATASTORES = {}

    def __init__(self, json_file_path, init=False, overwrite=False):
        super(JsonStorageBackend, self).__init__()
        self._json_path = json_file_path
        if init:
            if os.path.isfile(json_file_path):
                if overwrite:
                    self._init()
            else:
                self._init()

    @property
    def json_path(self):
        return self._json_path

    def get_datastore_keys(self):
        datastores = self._load_json()['datastores']
        keys = list()
        for ds_name, ds_props in datastores.items():
            ds_cls = self.SUPPORTED_DATASTORES[ds_props["cls_name"]]
            keys.append((ds_name, ds_cls))
        return keys

    def get_datastore_key(self, datastore_name):
        props = self._load_json()['datastores'][datastore_name]
        ds_cls = self.SUPPORTED_DATASTORES[props["cls_name"]]
        return datastore_name, ds_cls

    def _init(self):
        """
        Init the json file.
        """
        storage_json = {
            "datastores": {},
        }
        self._write_json(storage_json)

    def _register(self, datastore, datastore_kwargs):
        props = self._load_json()
        props["datastores"][datastore.name] = {
            "kwargs": datastore_kwargs,
            "cls_name": datastore.__class__.__name__,
        }
        self._write_json(props)

    def load_datastore(self, datastore_name):
        return self._load_json()["datastores"][datastore_name]

    def _load_json(self):
        with open(self.json_path, 'r') as props:
            json_str = props.read()
            return json.loads(json_str)

    def _write_json(self, properties):
        with open(self.json_path, 'w') as props:
            json.dump(properties, props)
