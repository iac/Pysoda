# coding: utf-8


def make_json_storage_backend(path, init=True, overwrite=False):
    from .json_storage_backend import JsonStorageBackend
    return JsonStorageBackend(path, init=init, overwrite=overwrite)


def make_django_storage_backend():
    from .django_storage_backend import DjangoStorageBackend
    return DjangoStorageBackend()
