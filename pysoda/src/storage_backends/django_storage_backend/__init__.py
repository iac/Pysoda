# coding: utf-8

import json

from ..storage_backend_base import StorageBackend


class DjangoStorageBackend(StorageBackend):
    """
    Storage backend using django's orm to allow integration with a django
    application.
    """

    SUPPORTED_DATASTORES = {}

    def __init__(self):
        super(DjangoStorageBackend, self).__init__()
        from .models import DjangoStorageBackendModel
        self.model = DjangoStorageBackendModel

    def get_datastore_key(self, datastore_name):
        datastore = self.model.objects.get(datastore_name=datastore_name)
        cls = self.SUPPORTED_DATASTORES[datastore.cls_name]
        return datastore.datastore_name, cls

    def get_datastore_keys(self):
        keys = list()
        for ds in self.model.objects.all():
            cls = self.SUPPORTED_DATASTORES[ds.cls_name]
            keys.append((ds.datastore_name, cls))
        return keys

    def _register(self, datastore, datastore_kwargs):
        ds = self.model(
            datastore_name=datastore.name,
            cls_name=datastore.__class__.__name__,
            kwargs=json.dumps({'kwargs': datastore_kwargs}),
        )
        ds.save()

    def load_datastore(self, datastore_name):
        kwargs = self.model.objects.get(datastore_name=datastore_name).kwargs
        return json.loads(kwargs)
