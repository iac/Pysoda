# coding: utf-8

from django.db import models


class DjangoStorageBackendModel(models.Model):
    """
    Django model used for storing the datastores.
    """

    datastore_name = models.CharField(primary_key=True, max_length=155)
    cls_name = models.CharField(max_length=155)
    kwargs = models.TextField()
