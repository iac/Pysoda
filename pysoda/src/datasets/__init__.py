# coding: utf-8


def load_datasets():
    import pysoda.src.datasets.mock_dataset
    import pysoda.src.datasets.table_dataset
    import pysoda.src.datasets.typed_table_dataset
    import pysoda.src.datasets.geo_table_dataset


# ========================= #
# Dataset factory functions #
# ========================= #

def make_mock_dataset(name):
    """
    Factory function for MockDataset.
    :param name: The name of the dataset to create.
    :return: The created MockDataset.
    """
    from .mock_dataset import MockDatasetFactory
    return MockDatasetFactory.create_dataset(name)


def make_table_dataset(name, header):
    """
    :param name: The name of the table dataset to create.
    :param header: The header (columns) of the table.
    :return: The created TableDataset.
    """
    from .table_dataset import TableDatasetFactory
    return TableDatasetFactory.create_dataset(name, header)


def make_table_write_dataset(name, header):
    """
    :param name: The name of the table write dataset to create.
    :param header: The header (columns) of the table.
    :return: The created TableWriteDataset.
    """
    from .table_dataset import TableWriteDatasetFactory
    return TableWriteDatasetFactory.create_dataset(name, header)


def make_typed_table_dataset(name, header):
    """
    :param name: The name of the typed table dataset to create.
    :param header: The header - (column_name, column_type) - of the table.
    :return: The created TypedTableDataset.
    """
    from .typed_table_dataset import TypedTableDatasetFactory
    return TypedTableDatasetFactory.create_dataset(name, header)


def make_typed_table_write_dataset(name, header):
    """
    :param name: The name of the typed table write dataset to create.
    :param header: The header - (column_name, column_type) - of the table.
    :return: The created TypedTableWriteDataset.
    """
    from .typed_table_dataset import TypedTableWDatasetFactory
    return TypedTableWDatasetFactory.create_dataset(name, header)


def make_geo_table_dataset(name, header):
    """
    :param name: The name of the geo table dataset to create.
    :param header: The header - (column_name, column_type) - of the table.
    :return: The created TypedTableDataset.
    """
    from .geo_table_dataset import GeoTableDatasetFactory
    return GeoTableDatasetFactory.create_dataset(name, header)
