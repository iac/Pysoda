# coding: utf-8

from ...datasets.dataset_base import Dataset
from ...dataset_services import TableReadService, TableWriteService


class TableDataset(Dataset, TableReadService):
    """
    Dataset representing a row-oriented table, in its simplest form:
    - Columns are not typed, everything is str.
    - No constraints on cells and rows, they can be empty, duplicates, ...
    - Column must be named.
    """

    SERVICES = [TableReadService, ]

    def __init__(self, name, header):
        super(TableDataset, self).__init__(name)
        self._header = header

    def provided_services(self):
        return self.SERVICES

    def get_header(self, exclude=[]):
        header = list()
        for h in self._header:
            if isinstance(h, (list, tuple)):
                if h[0] not in exclude:
                    header.append(h)
            else:
                if h not in exclude:
                    header.append(h)
        return tuple(header)

    def get_rows(self, exclude=[], filters={}):
        return self.dialect.get_rows(self, exclude=exclude, filters=filters)


class TableWriteDataset(TableDataset, TableWriteService):
    """
    Table dataset with write enabled.
    """

    SERVICES = TableDataset.SERVICES + [TableWriteService, ]

    def provided_services(self):
        return self.SERVICES

    def append_row(self, row):
        self.dialect.append_row(self, row)

    def append_rows(self, rows):
        if hasattr(self.dialect, "append_rows"):
            self.dialect.append_rows(self, rows)
        else:
            super(TableDataset, self).append_rows(rows)
