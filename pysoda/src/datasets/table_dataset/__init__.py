# coding: utf-8

from ...datasets.dataset_base import DatasetFactory, DATASET_FACTORIES
from .dataset import TableDataset, TableWriteDataset
from ...datastores import *
from .dialects import TableDatasetMockDatastoreDialect,\
    TableDatasetCsvFolderDialect, TableWDatasetMockDatastoreDialect,\
    TableWDatasetCsvFolderDialect, TableDatasetSQLDialect,\
    TableWDatasetSQLDialect


DIALECTS = {
    MockDatastore: TableDatasetMockDatastoreDialect,
    CsvFolderDatastore: TableDatasetCsvFolderDialect,
    SQLDatastore: TableDatasetSQLDialect,
}


class TableDatasetFactory(DatasetFactory):
    @staticmethod
    def create_dataset(name, header):
        return TableDataset(name, header)

    @classmethod
    def get_dialect(cls, datastore):
        return DIALECTS[datastore.__class__](datastore)


DATASET_FACTORIES[TableDataset] = TableDatasetFactory

cls_name = TableDataset.__name__

MockDatastore.SUPPORTED_DATASETS[cls_name] = TableDataset
CsvFolderDatastore.SUPPORTED_DATASETS[cls_name] = TableDataset
SQLDatastore.SUPPORTED_DATASETS[cls_name] = TableDataset


# Table write dataset factory

WRITE_DIALECTS = {
    MockDatastore: TableWDatasetMockDatastoreDialect,
    CsvFolderDatastore: TableWDatasetCsvFolderDialect,
    SQLDatastore: TableWDatasetSQLDialect,
}


class TableWriteDatasetFactory(DatasetFactory):
    @staticmethod
    def create_dataset(name, header):
        return TableWriteDataset(name, header)

    @classmethod
    def get_dialect(cls, datastore):
        return WRITE_DIALECTS[datastore.__class__](datastore)


DATASET_FACTORIES[TableWriteDataset] = TableWriteDatasetFactory

cls_write_name = TableWriteDataset.__name__

MockDatastore.SUPPORTED_DATASETS[cls_write_name] = TableWriteDataset
CsvFolderDatastore.SUPPORTED_DATASETS[cls_write_name] = TableWriteDataset
SQLDatastore.SUPPORTED_DATASETS[cls_write_name] = TableWriteDataset
