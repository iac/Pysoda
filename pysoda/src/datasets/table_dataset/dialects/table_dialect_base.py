# coding: utf-8

import abc

from ....datasets.dataset_base import DatasetDialect
from ..dataset import TableDataset, TableWriteDataset


class TableDatasetDialect(DatasetDialect):
    """
    Abstract base class for table dataset dialect.
    """

    def load_dataset(self, dataset_name):
        return TableDataset(dataset_name, self.load_header(dataset_name))

    @abc.abstractmethod
    def register_dataset(self, dataset):
        pass

    @abc.abstractmethod
    def load_header(self, dataset_name):
        pass

    @abc.abstractmethod
    def get_rows(self, dataset, exclude=[], filters={}):
        pass


class TableWriteDatasetDialect(DatasetDialect):
    """
    Abstract base class for table dataset dialect.
    """

    def load_dataset(self, dataset_name):
        return TableWriteDataset(
            dataset_name,
            self.load_header(dataset_name)
        )

    @abc.abstractmethod
    def append_row(self, dataset, row):
        pass
