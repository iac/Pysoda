# coding: utf-8

import numpy as np

from .table_dialect_base import TableDatasetDialect, TableWriteDatasetDialect


class TableDatasetMockDatastoreDialect(TableDatasetDialect):
    """
    Table dataset dialect for mock datastore.
    """

    def load_header(self, dataset_name):
        return self.datastore.datasets_contents[dataset_name]["header"]

    def register_dataset(self, dataset):
        self.datastore.datasets.append(dataset)
        header = dataset.get_header()
        rows = list()
        if dataset.dialect is not None:
            rows = dataset.get_rows()
        self.datastore.datasets_contents[dataset.name] = {
            'header': header,
            'rows': rows,
        }

    def get_rows(self, dataset, exclude=[], filters={}):
        rows = self.datastore.datasets_contents[dataset.name]["rows"]
        # Filter
        if len(filters) > 0:
            idx_filters = {dataset.get_column_index(k): filters[k]
                           for k in filters}
            filtered = list()
            for row in rows:
                b = True
                for i, v in idx_filters.items():
                    if row[i] != v:
                        b = False
                        continue
                if b:
                    filtered.append(row)
            rows = filtered
        # Exclude
        if len(exclude) > 0:
            exclude_idx = list()
            for i, h in enumerate(dataset.get_header()):
                # Typed table case TODO: Make it less hackish
                if isinstance(h, (list, tuple)):
                    if h[0] in exclude:
                        exclude_idx.append(i)
                else:
                    if h in exclude:
                        exclude_idx.append(i)
            array = np.array(rows)
            rows = list(map(tuple, np.delete(array, exclude_idx, 1)))
        return rows


class TableWDatasetMockDatastoreDialect(TableWriteDatasetDialect,
                                        TableDatasetMockDatastoreDialect):
    """
    Table write dataset dialect for mock datastore.
    """

    def append_row(self, dataset, row):
        self.datastore.datasets_contents[dataset.name]["rows"].append(row)
