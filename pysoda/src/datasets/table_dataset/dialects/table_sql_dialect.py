# coding: utf-8

from sqlalchemy import Column, String

from .table_dialect_base import TableDatasetDialect, TableWriteDatasetDialect


class TableDatasetSQLDialect(TableDatasetDialect):
    """
    Table dataset dialect for sql datastore.
    """

    def register_dataset(self, dataset):
        columns = [Column(h, String) for h in dataset.get_header()]
        self.datastore.create_table(dataset.name, columns)
        self.datastore.register_(dataset, {})

    def load_header(self, dataset_name):
        table = self.datastore.get_table(dataset_name)
        return tuple([c.name for c in table.columns])

    def get_rows(self, dataset, exclude=[], filters={}):
        return self.datastore.get_table_rows(
            dataset.name,
            exclude=exclude,
            filters=filters,
        )


class TableWDatasetSQLDialect(TableWriteDatasetDialect,
                              TableDatasetSQLDialect):
    """
    Table write dataset dialect for sql datastore.
    """

    def append_row(self, dataset, row):
        self.datastore.insert_row(dataset.name, row)
