# coding: utf-8

import numpy as np

from .table_dialect_base import TableDatasetDialect, TableWriteDatasetDialect


class TableDatasetCsvFolderDialect(TableDatasetDialect):
    """
    Table dataset dialect for csv folder datastore.
    """

    @staticmethod
    def get_filename(dataset):
        return "{}.csv".format(dataset.name)

    def load_header(self, dataset_name):
        kwargs = self.datastore.get_dataset_kwargs(dataset_name)
        return tuple(kwargs["header"])

    def register_dataset(self, dataset):
        filename = self.get_filename(dataset)
        header = dataset.get_header()
        self.datastore._register(dataset, {
            'header': header,
            'filename': filename,
        })
        rows = list()
        if dataset.dialect is not None:
            rows = dataset.get_rows()
        self.datastore.write_csv_file(filename, rows, header)

    def get_rows(self, dataset, exclude=[], filters={}):
        filename = self.get_filename(dataset)
        rows = self.datastore.load_csv_file(filename, has_header=True)[1]
        # Filter
        if len(filters) > 0:
            idx_filters = {dataset.get_column_index(k): filters[k]
                           for k in filters}
            filtered = list()
            for row in rows:
                b = True
                for i, v in idx_filters.items():
                    if row[i] != v:
                        b = False
                        continue
                if b:
                    filtered.append(row)
            rows = filtered
        # Exclude
        if len(exclude) > 0:
            exclude_idx = [dataset.get_column_index(k) for k in exclude]
            array = np.array(rows)
            rows = list(map(tuple, np.delete(array, exclude_idx, 1)))
        return rows

class TableWDatasetCsvFolderDialect(TableWriteDatasetDialect,
                                    TableDatasetCsvFolderDialect):
    """
    Table write dataset dialect for csv folder datastore.
    """

    def append_row(self, dataset, row):
        rows = dataset.get_rows()
        rows.append(row)
        header = dataset.get_header()
        filename = self.get_filename(dataset)
        self.datastore.write_csv_file(filename, rows, header=header)
