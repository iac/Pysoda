# coding: utf-8

from .typed_table_dialect_base import TypedTableDatasetDialect,\
    TypedTableWDatasetDialect
from ...table_dataset.dialects import TableDatasetMockDatastoreDialect,\
    TableWDatasetMockDatastoreDialect


class TypedTableDatasetMockDatastoreDialect(TypedTableDatasetDialect,
                                            TableDatasetMockDatastoreDialect):
    """
    Typed Table dataset dialect for mock datastore.
    """


class TypedTableWDatasetMockDatastoreDialect(
        TypedTableWDatasetDialect,
        TypedTableDatasetMockDatastoreDialect,
        TableWDatasetMockDatastoreDialect):
    """
    Table write dataset dialect for mock datastore.
    """
