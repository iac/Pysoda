# coding: utf-8

from .typed_table_dialect_base import TypedTableDatasetDialect,\
    TypedTableWDatasetDialect
from ...table_dataset.dialects import TableDatasetCsvFolderDialect,\
    TableWDatasetCsvFolderDialect
from ...utils.type_mappings import *


def from_str(type_, value):
    if type_ == bool:
        return True if value == "True" else False
    else:
        return type_(value)


class TypedTableDatasetCsvFolderDialect(TypedTableDatasetDialect,
                                        TableDatasetCsvFolderDialect):
    """
    Typed table dataset dialect for csv folder datastore.
    """

    def load_header(self, dataset_name):
        header = TableDatasetCsvFolderDialect.load_header(self, dataset_name)
        return tuple([tuple(col) for col in header])

    def get_rows(self, dataset, exclude=[], filters={}):
        rows = TableDatasetCsvFolderDialect.get_rows(self, dataset,
                                                     exclude=exclude,
                                                     filters=filters)
        typed_rows = list()
        header = dataset.get_header(exclude=exclude)
        types = PYTHON_TYPE_MAPPING
        for row in rows:
            typed_row = list()
            for i, cell in enumerate(row):
                t = types[header[i][1]]
                typed_row.append(from_str(t, cell))
            typed_rows.append(tuple(typed_row))
        return typed_rows


class TypedTableWDatasetCsvFolderDialect(TypedTableWDatasetDialect,
                                         TypedTableDatasetCsvFolderDialect,
                                         TableWDatasetCsvFolderDialect):
    """
    Typed table write dataset dialect for csv folder datastore.
    """
