# coding: utf-8

from sqlalchemy import Column, String, Integer, Float, Boolean, Date,\
    DateTime, Time

from .typed_table_dialect_base import TypedTableDatasetDialect,\
    TypedTableWDatasetDialect
from ...table_dataset.dialects import TableDatasetSQLDialect,\
    TableWDatasetSQLDialect
from ...utils.type_mappings import *


class TypedTableDatasetSQLDialect(TypedTableDatasetDialect,
                                  TableDatasetSQLDialect):
    """
    Typed table dataset dialect for sql datastore.
    """

    SQL_ALCHEMY_TYPE_MAPPING = {
        INTEGER_TYPE: Integer,
        FLOAT_TYPE: Float,
        VARCHAR_TYPE: String,
        BOOLEAN_TYPE: Boolean,
        DATE_TYPE: Date,
        DATETIME_TYPE: DateTime,
        TIME_TYPE: Time,
    }

    def register_dataset(self, dataset):
        columns = [Column(hn, self.SQL_ALCHEMY_TYPE_MAPPING[ht])
                   for hn, ht in dataset.get_header()]
        self.datastore.create_table(dataset.name, columns)
        self.datastore.register_(dataset, {})

    def load_header(self, dataset_name):
        table = self.datastore.get_table(dataset_name)
        return tuple([(c.name, PYTHON_TYPE_MAPPING_REV[c.type.python_type])
                      for c in table.columns])

    def get_rows(self, dataset, exclude=[], filters={}):
        return TableDatasetSQLDialect.get_rows(
            self,
            dataset,
            exclude=exclude,
            filters=filters,
        )


class TypedTableWDatasetSQLDialect(TypedTableWDatasetDialect,
                                   TypedTableDatasetSQLDialect,
                                   TableWDatasetSQLDialect):
    """
    Table write dataset dialect for sql datastore.
    """
