# coding: utf-8

from ....datasets.table_dataset.dialects import TableDatasetDialect,\
    TableWriteDatasetDialect
from ..dataset import TypedTableDataset, TypedTableWriteDataset


class TypedTableDatasetDialect(TableDatasetDialect):
    """
    Abstract base class for typed table dataset dialect.
    """

    def load_dataset(self, dataset_name):
        return TypedTableDataset(dataset_name, self.load_header(dataset_name))


class TypedTableWDatasetDialect(TableWriteDatasetDialect):
    """
    Abstract base class for typed table write dataset dialect.
    """

    def load_dataset(self, dataset_name):
        return TypedTableWriteDataset(
            dataset_name,
            self.load_header(dataset_name)
        )
