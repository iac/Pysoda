# coding: utf-8

from ...datasets.dataset_base import DatasetFactory, DATASET_FACTORIES
from .dataset import TypedTableDataset, TypedTableWriteDataset
from ...datastores import *
from .dialects import TypedTableDatasetMockDatastoreDialect,\
    TypedTableDatasetCsvFolderDialect, TypedTableDatasetSQLDialect,\
    TypedTableWDatasetMockDatastoreDialect,\
    TypedTableWDatasetCsvFolderDialect, TypedTableWDatasetSQLDialect


DIALECTS = {
    MockDatastore: TypedTableDatasetMockDatastoreDialect,
    CsvFolderDatastore: TypedTableDatasetCsvFolderDialect,
    SQLDatastore: TypedTableDatasetSQLDialect,
}


class TypedTableDatasetFactory(DatasetFactory):
    @staticmethod
    def create_dataset(name, header):
        return TypedTableDataset(name, header)

    @classmethod
    def get_dialect(cls, datastore):
        return DIALECTS[datastore.__class__](datastore)


DATASET_FACTORIES[TypedTableDataset] = TypedTableDatasetFactory

cls_name = TypedTableDataset.__name__

MockDatastore.SUPPORTED_DATASETS[cls_name] = TypedTableDataset
CsvFolderDatastore.SUPPORTED_DATASETS[cls_name] = TypedTableDataset
SQLDatastore.SUPPORTED_DATASETS[cls_name] = TypedTableDataset


# Write

WRITE_DIALECTS = {
    MockDatastore: TypedTableWDatasetMockDatastoreDialect,
    CsvFolderDatastore: TypedTableWDatasetCsvFolderDialect,
    SQLDatastore: TypedTableWDatasetSQLDialect,
}


class TypedTableWDatasetFactory(DatasetFactory):
    @staticmethod
    def create_dataset(name, header):
        return TypedTableWriteDataset(name, header)

    @classmethod
    def get_dialect(cls, datastore):
        return WRITE_DIALECTS[datastore.__class__](datastore)


DATASET_FACTORIES[TypedTableWriteDataset] = TypedTableWDatasetFactory

cls_name_w = TypedTableWriteDataset.__name__

MockDatastore.SUPPORTED_DATASETS[cls_name_w] = TypedTableWriteDataset
CsvFolderDatastore.SUPPORTED_DATASETS[cls_name_w] = TypedTableWriteDataset
SQLDatastore.SUPPORTED_DATASETS[cls_name_w] = TypedTableWriteDataset
