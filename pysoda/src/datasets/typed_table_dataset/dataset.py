# coding: utf-8

from ..table_dataset import TableDataset, TableWriteDataset
from ..utils.type_mappings import STANDARD_TYPES


class TypedTableDataset(TableDataset):
    """
    Extension of the table dataset, where the columns are typed.
    The TypedTableDataset must be instantiated as the TableDataset, ie with
    a header tuple, but for this extension each element of the tuple is a
    couple of the column name and the column type name.
    Available types are: Varchar, Integer, Float and Boolean.
    """

    SERVICES = TableDataset.SERVICES

    def __init__(self, name, header):
        self._check_header(header)
        super(TypedTableDataset, self).__init__(name, header)

    @staticmethod
    def _check_header(header):
        cols = dict()
        for col_name, col_type in header:
            if col_name in cols:
                msg = "The column {} is defined twice."
                raise ValueError(msg.format(col_name))
            if col_type not in STANDARD_TYPES:
                msg = "The column type {} is not defined"
                raise ValueError(msg.format(col_type))
            cols[col_name] = True

    def get_header_names(self, exclude=[]):
        return [h[0] for h in self.get_header(exclude=exclude)]


class TypedTableWriteDataset(TypedTableDataset, TableWriteDataset):
    """
    Typed table dataset with write enabled.
    """

    def __init__(self, name, header):
        super(TableWriteDataset, self).__init__(name, header)
