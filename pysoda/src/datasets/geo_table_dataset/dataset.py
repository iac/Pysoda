# coding: utf-8

from ...dataset_services.geo_table_service import GeoTableService
from ..typed_table_dataset import TypedTableDataset
from ..utils.type_mappings import *


class GeoTableDataset(TypedTableDataset, GeoTableService):
    """
    Extension of the typed table dataset where the geometry types are
    supported.
    """

    SERVICES = [GeoTableService, ]

    @staticmethod
    def _check_header(header):
        cols = dict()
        for col_name, col_type in header:
            type_name = col_type
            args = []
            try:
                type_name, args = parse_field_declaration(col_type)
            except:
                pass
            if col_name in cols:
                msg = "The column {} is defined twice."
                raise ValueError(msg.format(col_name))
            if type_name not in ALL_TYPES:
                msg = "The column type {} is not defined"
                raise ValueError(msg.format(col_type))
            cols[col_name] = True

    def to_shape(self, geometry_object):
        return self.dialect.to_shape(geometry_object)

    def find_geometry_column(self):
        for col_name, col_type in self.get_header():
            try:
                type_name, args = parse_field_declaration(col_type)
                if type_name in GEOMETRY_TYPES:
                    return col_name
            except:
                if col_type in GEOMETRY_TYPES:
                    return col_name
        raise ValueError('No geometry column found.')
