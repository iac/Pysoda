# coding: utf-8

from sqlalchemy import Column
from geoalchemy2 import Geometry
from geoalchemy2.shape import to_shape
from sqlalchemy.dialects.postgresql.base import UUID

from .geo_table_dialect_base import GeoTableDatasetDialect
from ...typed_table_dataset import TypedTableDatasetSQLDialect
from ...utils.type_mappings import *


class GeoTableDatasetSQLDialect(GeoTableDatasetDialect,
                                TypedTableDatasetSQLDialect):
    """
    Geo table dataset dialect for sql datastore.
    """

    STANDARD_MAPPING = TypedTableDatasetSQLDialect.SQL_ALCHEMY_TYPE_MAPPING
    GEO_MAPPING = {
        POINT_TYPE: Geometry('POINT'),
        POLYGON_TYPE: Geometry('POLYGON'),
        MULTIPOLYGON_TYPE: Geometry('MULTIPOLYGON'),
    }
    GEO_MAPPING.update(STANDARD_MAPPING)

    GEO_REV_MAPPING = {
        'POINT': POINT_TYPE,
        'POLYGON': POLYGON_TYPE,
        'MULTIPOLYGON': MULTIPOLYGON_TYPE,
    }

    def register_dataset(self, dataset):
        columns = list()
        for hn, ht in dataset.get_header():
            try:
                type_name, args = parse_field_declaration(ht)
            except TypeError:
                type_name = ht
            columns.append(Column(hn, self.GEO_MAPPING[type_name]))
        self.datastore.create_table(dataset.name, columns)
        self.datastore.register_(dataset, {})

    def load_header(self, dataset_name):
        table = self.datastore.get_table(dataset_name)
        header = list()
        for c in table.columns:
            if isinstance(c.type, Geometry):
                g_type = c.type.geometry_type
                py_type = self.GEO_REV_MAPPING[g_type]
            elif isinstance(c.type, UUID):
                py_type = 'uuid'
            else:
                py_type = PYTHON_TYPE_MAPPING_REV[c.type.python_type]
            couple = (c.name, py_type)
            header.append(couple)
        return tuple(header)

    def to_shape(self, geometry_object):
        return to_shape(geometry_object)
