# coding: utf-8

import abc

from ....datasets.typed_table_dataset.dialects import TypedTableDatasetDialect
from ..dataset import GeoTableDataset


class GeoTableDatasetDialect(TypedTableDatasetDialect):
    """
    Abstract base class for geo table dataset dialect.
    """

    def load_dataset(self, dataset_name):
        return GeoTableDataset(dataset_name, self.load_header(dataset_name))

    @abc.abstractmethod
    def to_shape(self, geometry_object):
        """
        :param geometry_object: The geometry object
        :return: A shapely shape a technology specific geometry object.
        """
