# coding: utf-8

from ...datasets.dataset_base import DatasetFactory, DATASET_FACTORIES
from .dataset import GeoTableDataset
from ...datastores import *
from .dialects import GeoTableDatasetSQLDialect


DIALECTS = {
    SQLDatastore: GeoTableDatasetSQLDialect,
}


class GeoTableDatasetFactory(DatasetFactory):
    @staticmethod
    def create_dataset(name, header):
        return GeoTableDataset(name, header)

    @classmethod
    def get_dialect(cls, datastore):
        return DIALECTS[datastore.__class__](datastore)


DATASET_FACTORIES[GeoTableDataset] = GeoTableDatasetFactory

cls_name = GeoTableDataset.__name__

SQLDatastore.SUPPORTED_DATASETS[cls_name] = GeoTableDataset
