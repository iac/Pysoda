# coding: utf-8

import abc

from ...datasets.dataset_base import DatasetDialect
from .dataset import MockDataset


class MockDatasetDialect(DatasetDialect):
    def load_dataset(self, dataset_name):
        return MockDataset(dataset_name)

    @abc.abstractmethod
    def register_dataset(self, dataset):
        pass

    @abc.abstractmethod
    def bark(self):
        pass

    @abc.abstractmethod
    def cocorico(self):
        pass

    @abc.abstractmethod
    def meow(self):
        pass


class MockDatasetMockDatastoreDialect(MockDatasetDialect):
    def register_dataset(self, dataset):
        self.datastore.datasets.append(dataset)

    def meow(self):
        print("Mock meeeeooowww!")

    def bark(self):
        print("Mock waf, mock waf!!")

    def cocorico(self):
        print("Cocorimooooooock!!")


class MockDatasetCsvFolderDialect(MockDatasetDialect):
    def register_dataset(self, dataset):
        self.datastore._register(dataset, {"name": dataset.name})

    def meow(self):
        print('CsvMeeeeoooooow!!')

    def bark(self):
        print("CsvWaf, CsvWaf!!")

    def cocorico(self):
        print("Csvcocoricoooo!!!")


class MockDatasetSQLDialect(MockDatasetDialect):
    def register_dataset(self, dataset):
        self.datastore.register_(dataset, {})

    def meow(self):
        print('SQLmeow')

    def bark(self):
        print('SQLwaf')

    def cocorico(self):
        print('SQLcocorico')
