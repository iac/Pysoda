# coding: utf-8

from ...datasets.dataset_base import Dataset
from ...dataset_services import MockService


SERVICES = [MockService, ]
# INHERITS = [Dataset, MockService]


class MockDataset(Dataset, MockService):
    def provided_services(self):
        return SERVICES

    def meow(self):
        self.dialect.meow()

    def bark(self):
        self.dialect.bark()

    def cocorico(self):
        self.dialect.cocorico()
