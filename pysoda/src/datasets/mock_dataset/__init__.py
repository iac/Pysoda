# coding: utf-8

from ...datasets.dataset_base import DatasetFactory, DATASET_FACTORIES
from .dataset import MockDataset
from ...datastores import *
from .dialects import MockDatasetMockDatastoreDialect,\
    MockDatasetCsvFolderDialect, MockDatasetSQLDialect


DIALECTS = {
    MockDatastore: MockDatasetMockDatastoreDialect,
    CsvFolderDatastore: MockDatasetCsvFolderDialect,
    SQLDatastore: MockDatasetSQLDialect,
}


class MockDatasetFactory(DatasetFactory):
    @staticmethod
    def create_dataset(name):
        return MockDataset(name)

    @classmethod
    def get_dialect(cls, datastore):
        return DIALECTS[datastore.__class__](datastore)


DATASET_FACTORIES[MockDataset] = MockDatasetFactory

cls_name = MockDataset.__name__

MockDatastore.SUPPORTED_DATASETS[cls_name] = MockDataset
CsvFolderDatastore.SUPPORTED_DATASETS[cls_name] = MockDataset
SQLDatastore.SUPPORTED_DATASETS[cls_name] = MockDataset
