# coding: utf-8

import abc


DATASET_FACTORIES = {}


class DatasetFactory(abc.ABC):
    """
    Abstract factory for datasets.
    """
    @classmethod
    def load_dataset(cls, dataset_name, datastore):
        """
        Make a dataset.
        """
        dialect = cls.get_dialect(datastore)
        dataset = dialect.load_dataset(dataset_name)
        dataset.dialect = dialect
        return dataset

    @staticmethod
    @abc.abstractmethod
    def create_dataset(*args, **kwargs):
        """
        Create an unregistered dataset.
        """

    @classmethod
    @abc.abstractmethod
    def get_dialect(cls, datastore):
        """
        Return the corresponding dialect for the given datastore.
        """


class Dataset(abc.ABC):
    """
    Abstract base class for representing a dataset.
    """
    def __init__(self, name, dialect=None):
        self._name = name
        self._dialect = dialect

    @property
    def name(self):
        return self._name

    @property
    def dialect(self):
        return self._dialect

    @dialect.setter
    def dialect(self, dialect):
        self._dialect = dialect

    @abc.abstractmethod
    def provided_services(self):
        """
        :return: The list of the services provided by this dataset.
        """


class DatasetDialect(abc.ABC):
    """
    Abstract base class for dataset datastore dialect.
    """
    def __init__(self, datastore):
        self._datastore = datastore

    @property
    def datastore(self):
        return self._datastore

    @abc.abstractmethod
    def load_dataset(self, dataset_name):
        """
        :param dataset_name: The name of the dataset to load.
        :return: The loaded dataset.
        """

    @abc.abstractmethod
    def register_dataset(self, dataset):
        """
        :param dataset: The dataset to register.
        """
