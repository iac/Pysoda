# coding: utf-8

import re
from datetime import date, datetime, time

from shapely.geometry import *

# "Standard" types
VARCHAR_TYPE = 'varchar'
INTEGER_TYPE = 'integer'
FLOAT_TYPE = 'float'
BOOLEAN_TYPE = 'boolean'
DATE_TYPE = 'date'
DATETIME_TYPE = 'datetime'
TIME_TYPE = 'time'

# Extra types
UUID_TYPE = 'uuid'
ARRAY_TYPE = 'array'

# Geometry types
# Shapely library is used for vector types
POINT_TYPE = 'point'
POLYGON_TYPE = 'polygon'
MULTIPOLYGON_TYPE = 'multipolygon'

PYTHON_TYPE_MAPPING = {
    VARCHAR_TYPE: str,
    INTEGER_TYPE: int,
    FLOAT_TYPE: float,
    BOOLEAN_TYPE: bool,
    DATE_TYPE: date,
    DATETIME_TYPE: datetime,
    TIME_TYPE: time,
    POINT_TYPE: Point,
    POLYGON_TYPE: Polygon,
    MULTIPOLYGON_TYPE: MultiPolygon,
    ARRAY_TYPE: list,
}

PYTHON_TYPE_MAPPING_REV = {v: k for k, v in PYTHON_TYPE_MAPPING.items()}

PYTHON_TYPE_MAPPING[UUID_TYPE] = str

STANDARD_TYPES = (VARCHAR_TYPE, INTEGER_TYPE, FLOAT_TYPE, BOOLEAN_TYPE,
                  DATE_TYPE, DATETIME_TYPE, TIME_TYPE, UUID_TYPE, ARRAY_TYPE)
GEOMETRY_TYPES = (POINT_TYPE, POLYGON_TYPE, MULTIPOLYGON_TYPE)

ALL_TYPES = STANDARD_TYPES + GEOMETRY_TYPES


def parse_field_declaration(declaration):
    match = re.search(r'^(\S*)\((.*)\)$', declaration)
    if match:
        result = match.groups()
        if len(result) != 2:
            raise TypeError('The field declaration is incorrect')
        name = result[0]
        args = [arg.rstrip().lstrip() for arg in result[1].split(',')]
        for arg in args:
            if not re.match(r'^(\S*)$', arg):
                raise TypeError('Argument {} is incorrect'.format(arg))
        return name, args
    raise TypeError('The field declaration is incorrect')
