# coding: utf-8

import abc


class TestDataset(abc.ABC):
    """
    Abstract base class for testing datasets.
    """
    @property
    def factory(self):
        pass

