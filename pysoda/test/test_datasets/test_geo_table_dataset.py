# coding: utf-8

import unittest
import os

import sqlalchemy

from pysoda.src.datasets import make_geo_table_dataset
from pysoda.src.datastores.sql_datastore import SQLDatastore


BASE_PATH = os.path.dirname(__file__)


class TestGeoTableDataset(unittest.TestCase):

    PG_PASSWORD = "postgres"
    HOST = "localhost"
    PG_URL = "postgresql://postgres:{}@{}/postgres".format(PG_PASSWORD, HOST)
    TEST_DATABASE_NAME = "pysoda_test"

    @classmethod
    def setUpClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("create database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()
        # Add postgis extension
        url = "postgresql://postgres:{}@{}/{}".format(
            cls.PG_PASSWORD,
            cls.HOST,
            cls.TEST_DATABASE_NAME,
        )
        engine2 = sqlalchemy.create_engine(url)
        conn2 = engine2.connect()
        conn2.execute("CREATE EXTENSION postgis;")
        conn2.close()

    @classmethod
    def tearDownClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{}'
            AND pid <> pg_backend_pid();
        """.format(cls.TEST_DATABASE_NAME))
        conn.execute("commit")
        conn.execute("drop database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    def init_datastore(self):
        url = "postgresql://postgres:{}@{}/{}".format(
            self.PG_PASSWORD,
            self.HOST,
            self.TEST_DATABASE_NAME,
        )
        self._datastore = SQLDatastore("pg_sql_datastore", url, init=True)

    def setUp(self):
        self.init_datastore()

    def tearDown(self):
        engine = sqlalchemy.create_engine(self.datastore.url)
        conn = engine.connect()
        tb_name = SQLDatastore.DATASETS_META_TABLE_NAME
        conn.execute("drop table {}".format(tb_name))
        conn.execute("commit")
        conn.close()

    def test_create(self):
        header = (('col_A', 'integer'),
                  ('col_B', 'varchar'),
                  ('col_C', 'point(32758)'),)
        geo_dataset = make_geo_table_dataset("geo_dataset", header)
        self.datastore.register_dataset(geo_dataset)
        dataset = self.datastore.get_dataset("geo_dataset")
        row = (1, "hello", "POINT(0 1)")
        self.datastore.insert_row("geo_dataset", row)
        try:
            # Export to csv
            path = os.path.join(BASE_PATH, 'temp/export/export.geojson')
            with open(path, 'w') as file:
                dataset.export('geojson', file, geometry_column='col_C')
        except:
            self.fail()

    @property
    def datastore(self):
        return self._datastore

    def tweak_row_insert(self, dataset, rows):
        # TODO Optimize
        for row in rows:
            self.datastore.insert_row(dataset.name, row)
        return True
