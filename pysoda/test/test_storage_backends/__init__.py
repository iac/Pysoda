# coding: utf-8

import unittest

from pysoda.test.test_storage_backends.test_json_storage_backend \
    import TestJsonStorageBackend


class TestSuiteStorageBackends(unittest.TestSuite):

    TESTS = [
        TestJsonStorageBackend(),
    ]

    def __init__(self):
        super(TestSuiteStorageBackends, self).__init__(self.TESTS)
