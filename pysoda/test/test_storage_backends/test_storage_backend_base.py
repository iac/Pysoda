# coding: utf-8

import abc

import sqlalchemy

from pysoda.src.datastores import *


# Is this even used?
DATASTORES = [
    MockDatastore,
    CsvFolderDatastore,
    SQLDatastore
]


class TestStorageBackend(abc.ABC):
    """
    Abstract base class for testing storage backends.
    """

    PG_PASSWORD = "postgres"
    HOST = "localhost"
    PG_URL = "postgresql://postgres:{}@{}/postgres".format(PG_PASSWORD, HOST)
    TEST_DATABASE_NAME = "pysoda_test"

    @classmethod
    def setUpClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("create database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    @classmethod
    def tearDownClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{}'
            AND pid <> pg_backend_pid();
        """.format(cls.TEST_DATABASE_NAME))
        conn.execute("commit")
        conn.execute("drop database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()


    @abc.abstractmethod
    def init_storage_backend(self):
        pass

    @property
    @abc.abstractmethod
    def storage_backend(self):
        pass

    def setUp(self):
        self.init_storage_backend()

    # Tests
    def test_get_datastore_keys_empty(self):
        keys = self.storage_backend.get_datastore_keys()
        self.assertEqual(len(keys), 0)

    def test_register_mock_datastore(self):
        mock_datastore = MockDatastore('mock_datastore')
        self.storage_backend.register_datastore(mock_datastore)
        datastores = self.storage_backend.get_datastores()
        self.assertEqual(len(datastores), 1)
        datastore = self.storage_backend.get_datastore('mock_datastore')
        self.assertIsNotNone(datastore)
        self.assertIsInstance(datastore, MockDatastore)

    def test_register_csv_folder_datastore(self):
        csv_folder_datastore = CsvFolderDatastore(
            'csv_folder_datastore',
            'test_storage_backends/temp/csv_folder',
            init=True
        )
        self.storage_backend.register_datastore(csv_folder_datastore)
        datastores = self.storage_backend.get_datastores()
        self.assertEqual(len(datastores), 1)
        datastore = self.storage_backend.get_datastore('csv_folder_datastore')
        self.assertIsNotNone(datastore)
        self.assertIsInstance(datastore, CsvFolderDatastore)

    def test_register_sql_datastore(self):
        url = "postgresql://postgres:{}@{}/{}".format(
            self.PG_PASSWORD,
            self.HOST,
            self.TEST_DATABASE_NAME,
        )
        sql_datastore = SQLDatastore("pg_sql_datastore", url, init=True)
        self.storage_backend.register_datastore(sql_datastore)
        datastores = self.storage_backend.get_datastores()
        self.assertEqual(len(datastores), 1)
        datastore = self.storage_backend.get_datastore('pg_sql_datastore')
        self.assertIsNotNone(datastore)
        self.assertIsInstance(datastore, SQLDatastore)
