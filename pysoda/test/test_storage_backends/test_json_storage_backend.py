# coding: utf-8

import unittest
import os

from pysoda.test.test_storage_backends.test_storage_backend_base import *
from pysoda.src.storage_backends import make_json_storage_backend


BASE_PATH = os.path.dirname(__file__)


class TestJsonStorageBackend(TestStorageBackend, unittest.TestCase):
    def tearDown(self):
        pass

    def init_storage_backend(self):
        self.path = os.path.join(BASE_PATH, "temp")
        self.json_path = os.path.join(self.path, "repo.json")
        self._storage_backend = make_json_storage_backend(self.json_path,
                                                          init=True,
                                                          overwrite=True)

    @property
    def storage_backend(self):
        return self._storage_backend
