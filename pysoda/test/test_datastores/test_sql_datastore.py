# coding: utf-8

import unittest

import sqlalchemy

from pysoda.test.test_datastores.test_datastores_base import *
from pysoda.src.datastores.sql_datastore import SQLDatastore


class TestPGSQLDatastore(TestDatastore, unittest.TestCase):
    """
    Test class for SQLDatastore using postgresql.
    """

    PG_PASSWORD = "postgres"
    HOST = "localhost"
    PG_URL = "postgresql://postgres:{}@{}/postgres".format(PG_PASSWORD, HOST)
    TEST_DATABASE_NAME = "pysoda_test"

    @classmethod
    def setUpClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("create database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()
        url = "postgresql://postgres:{}@{}/{}".format(
            cls.PG_PASSWORD,
            cls.HOST,
            cls.TEST_DATABASE_NAME,
        )
        engine2 = sqlalchemy.create_engine(url)
        conn2 = engine2.connect()
        conn2.execute("create extension postgis")
        conn2.close()

    @classmethod
    def tearDownClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{}'
            AND pid <> pg_backend_pid();
        """.format(cls.TEST_DATABASE_NAME))
        conn.execute("commit")
        conn.execute("drop database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    @property
    def datastore(self):
        return self._datastore

    def init_datastore(self):
        url = "postgresql://postgres:{}@{}/{}".format(
            self.PG_PASSWORD,
            self.HOST,
            self.TEST_DATABASE_NAME,
        )
        self._datastore = SQLDatastore("pg_sql_datastore", url, init=True)

    def tearDown(self):
        engine = sqlalchemy.create_engine(self.datastore.url)
        conn = engine.connect()
        tb_name = SQLDatastore.DATASETS_META_TABLE_NAME
        conn.execute("drop table {}".format(tb_name))
        conn.execute("commit")
        conn.close()
