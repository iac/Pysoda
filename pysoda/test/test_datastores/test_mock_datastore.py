# coding: utf-8

import unittest

from pysoda.test.test_datastores.test_datastores_base import *
from pysoda.src.datastores.mock_datastore import MockDatastore


class TestMockDatastore(TestDatastore, unittest.TestCase):

    def tearDown(self):
        pass

    def init_datastore(self):
        self._datastore = MockDatastore("mock_datastore")

    @property
    def datastore(self):
        return self._datastore
