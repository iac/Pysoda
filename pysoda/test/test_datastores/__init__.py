# coding: utf-8

import unittest

from pysoda.test.test_datastores.test_mock_datastore import TestMockDatastore
from pysoda.test.test_datastores.test_csv_folder_datastore \
    import TestCsvFolderDatastore


class TestSuiteDatastores(unittest.TestSuite):

    TESTS = [
        TestMockDatastore(),
        TestCsvFolderDatastore(),
    ]

    def __init__(self):
        super(TestSuiteDatastores, self).__init__(self.TESTS)
