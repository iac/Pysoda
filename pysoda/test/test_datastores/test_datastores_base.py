# coding: utf-8

import abc

from pysoda.src.datasets import *
from pysoda.src.datasets.mock_dataset import MockDataset
from pysoda.src.datasets.table_dataset import TableDataset, TableWriteDataset
from pysoda.src.datasets.typed_table_dataset import TypedTableDataset
from pysoda.src.datasets.geo_table_dataset import GeoTableDataset


DATASETS = {
    MockDataset: make_mock_dataset,
}


class TestDatastore(abc.ABC):
    """
    Abstract base class for testing datastores.
    """
    @abc.abstractmethod
    def init_datastore(self):
        pass

    @property
    @abc.abstractmethod
    def datastore(self):
        pass

    def setUp(self):
        self.init_datastore()

    # Tests
    def test_get_datasets_empty(self):
        keys = self.datastore.get_dataset_keys()
        datasets = self.datastore.get_datasets()
        self.assertEqual(len(keys), 0)
        self.assertEqual(len(datasets), 0)

    def test_register_mock_dataset(self):
        if MockDataset not in self.datastore.supported_datasets():
            return
        mock_dataset = make_mock_dataset("mock_dataset")
        self.datastore.register_dataset(mock_dataset)
        datasets = self.datastore.get_datasets()
        self.assertEqual(len(datasets), 1)
        dataset_key = self.datastore.get_dataset_key("mock_dataset")
        self.assertEqual(dataset_key, ("mock_dataset", MockDataset))
        dataset = self.datastore.get_dataset("mock_dataset")
        self.assertIsNotNone(dataset)
        self.assertIsInstance(dataset, MockDataset)

    def test_register_table_dataset(self):
        if TableDataset not in self.datastore.supported_datasets():
            return
        header = ("col_1", "col_2", "col_3")
        table_dataset = make_table_dataset("table_dataset", header)
        self.datastore.register_dataset(table_dataset)
        datasets = self.datastore.get_datasets()
        self.assertEqual(len(datasets), 1)
        dataset_key = self.datastore.get_dataset_key("table_dataset")
        self.assertEqual(dataset_key, ("table_dataset", TableDataset))
        dataset = self.datastore.get_dataset("table_dataset")
        self.assertIsNotNone(dataset)
        self.assertIsInstance(dataset, TableDataset)

    def test_register_table_write_dataset(self):
        if TableDataset not in self.datastore.supported_datasets():
            return
        header = ("col_1", "col_2", "col_3")
        table_w_dataset = make_table_write_dataset("table_w_dataset", header)
        self.datastore.register_dataset(table_w_dataset)
        datasets = self.datastore.get_datasets()
        self.assertEqual(len(datasets), 1)
        dataset_key = self.datastore.get_dataset_key("table_w_dataset")
        self.assertEqual(dataset_key, ("table_w_dataset", TableWriteDataset))
        dataset = self.datastore.get_dataset("table_w_dataset")
        self.assertIsNotNone(dataset)
        self.assertIsInstance(dataset, TableWriteDataset)

    def test_register_typed_table_dataset(self):
        if TypedTableDataset not in self.datastore.supported_datasets():
            return
        header = (("col_1", "integer"),
                  ("col_2", "varchar"),
                  ("col_3", "boolean"))
        typed_table = make_typed_table_dataset("typed_table", header)
        self.datastore.register_dataset(typed_table)
        datasets = self.datastore.get_datasets()
        self.assertEqual(len(datasets), 1)
        dataset_key = self.datastore.get_dataset_key("typed_table")
        self.assertEqual(dataset_key, ("typed_table", TypedTableDataset))
        dataset = self.datastore.get_dataset("typed_table")
        self.assertIsNotNone(dataset)
        self.assertIsInstance(dataset, TypedTableDataset)

    def test_register_geo_table_dataset(self):
        if GeoTableDataset not in self.datastore.supported_datasets():
            return
        header = (('col_A', 'integer'),
                  ('col_B', 'varchar'),
                  ('col_C', 'point(32758)'),
                  ('col_D', 'polygon(32758)'))
        geo_table = make_geo_table_dataset("geo_table", header)
        self.datastore.register_dataset(geo_table)
        datasets = self.datastore.get_datasets()
        self.assertEqual(len(datasets), 1)
        dataset_key = self.datastore.get_dataset_key("geo_table")
        self.assertEqual(dataset_key, ("geo_table", GeoTableDataset))
        dataset = self.datastore.get_dataset("geo_table")
        self.assertIsNotNone(dataset)
        self.assertIsInstance(dataset, GeoTableDataset)
