# coding: utf-8

import unittest
import os

from pysoda.test.test_datastores.test_datastores_base import *
from pysoda.src.datastores.csv_folder import CsvFolderDatastore


BASE_PATH = os.path.dirname(__file__)


class TestCsvFolderDatastore(TestDatastore, unittest.TestCase):
    def tearDown(self):
        pass

    def init_datastore(self):
        self._datastore = CsvFolderDatastore(
            'csv_folder_datastore',
            os.path.join(BASE_PATH, 'temp/csv_folder'),
            init=True
        )

    @property
    def datastore(self):
        return self._datastore
