# coding: utf-8

import abc
import unittest
import os

import sqlalchemy

from pysoda.src.datasets import *
from pysoda.src.datastores import *


BASE_PATH = os.path.dirname(__file__)


class TestTableService(abc.ABC):
    """
    Abstract base class testing table service.
    A test subclass must be implemented for each dataset/datastore couple.
    """

    @abc.abstractmethod
    def init_datastore(self):
        pass

    @property
    @abc.abstractmethod
    def datastore(self):
        pass

    @abc.abstractmethod
    def make_dataset(self, name, header):
        pass

    def setUp(self):
        self.init_datastore()

    # Tests
    def test_get_header(self, header=('col_A', 'col_B', 'col_C', 'col_D')):
        name = 'table_dataset'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        self.assertEqual(dataset.get_header(), header)

    def test_get_header_exclude(self, header=('col_A', 'col_B', 'col_C')):
        name = 'table_dataset_header_exclude'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        exclude_1 = ['col_A', ]
        header_1 = ('col_B', 'col_C')
        exclude_2 = ['col_C', 'col_B']
        header_2 = ('col_A', )
        self.assertEqual(dataset.get_header(exclude=exclude_1), header_1)
        self.assertEqual(dataset.get_header(exclude=exclude_2), header_2)

    def test_get_column_index(self, header=('col_A', 'col_B', 'col_C')):
        name = 'columns_dataset'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        self.assertEqual(dataset.get_column_index('col_A'), 0)
        self.assertEqual(dataset.get_column_index('col_B'), 1)
        self.assertEqual(dataset.get_column_index('col_C'), 2)

    def test_get_rows(self, header=('col_1', 'col_2')):
        name = 'table_dataset_rows'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        self.assertEqual(dataset.get_rows(), [])
        rows = [("1", "2"), ("a", "b"), ("hello", "jobijoba")]
        if self.tweak_row_insert(dataset, rows):
            self.assertEqual(dataset.get_rows(), rows)

    def test_get_rows_exclude(self, header=('col_1', 'col_2', 'col_3')):
        name = 'table_dataset_rows_exclude'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        self.assertEqual(dataset.get_rows(), [])
        rows = [("1", "2", "3"),
                ("a", "b", "c"),
                ("hello", "jobijoba", "proutiprout")]
        if self.tweak_row_insert(dataset, rows):
            exclude_1 = ['col_1', ]
            exclude_2 = ['col_3', 'col_2']
            rows_1 = [("2", "3"),
                      ("b", "c"),
                      ("jobijoba", "proutiprout")]
            rows_2 = [("1", ),
                      ("a", ),
                      ("hello", )]
            self.assertEqual(dataset.get_rows(exclude=exclude_1), rows_1)
            self.assertEqual(dataset.get_rows(exclude=exclude_2), rows_2)

    def test_get_rows_filters(self, header=('col_1', 'col_2')):
        name = 'table_dataset_rows_filters'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        self.assertEqual(dataset.get_rows(), [])
        rows = [("1", "2"), ("a", "b"), ("hello", "jobijoba")]
        filters = {'col_1': "1", }
        expected = [("1", "2"), ]
        if self.tweak_row_insert(dataset, rows):
            self.assertEqual(dataset.get_rows(filters=filters), expected)

    def test_export(self, header=('col_1', 'col_2')):
        name = 'table_dataset_export'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        rows = [("1", "2"), ("a", "b"), ("hello", "jobijoba")]
        if self.tweak_row_insert(dataset, rows):
            try:
                # Export to csv
                path = os.path.join(BASE_PATH, 'temp/export/export.csv')
                with open(path, 'w') as file:
                    dataset.export('csv', file)
            except:
                self.fail()
        else:
            print("dada")

    @abc.abstractmethod
    def tweak_row_insert(self, dataset, rows):
        return False


class TestTableServiceTableDataset(TestTableService):
    """
    Base class for testing table service with table dataset.
    """

    @abc.abstractmethod
    def init_datastore(self):
        pass

    @property
    @abc.abstractmethod
    def datastore(self):
        pass

    @abc.abstractmethod
    def tweak_row_insert(self, dataset, rows):
        return False

    def make_dataset(self, name, header):
        return make_table_dataset(name, header)


class TestTableServiceTypedTableDataset(TestTableService):
    """
    Base class for testing table service with typed table dataset.
    """

    @abc.abstractmethod
    def init_datastore(self):
        pass

    @property
    @abc.abstractmethod
    def datastore(self):
        pass

    @abc.abstractmethod
    def tweak_row_insert(self, dataset, rows):
        return False

    def make_dataset(self, name, header):
        return make_typed_table_dataset(name, header)


class TestTableMockDatastore(TestTableServiceTableDataset, unittest.TestCase):

    def init_datastore(self):
        self._datastore = MockDatastore("mock_datastore")

    @property
    def datastore(self):
        return self._datastore

    def tweak_row_insert(self, dataset, rows):
        self.datastore.datasets_contents[dataset.name]["rows"] = rows
        return True


class TestTypedTableMockDatastore(TestTableServiceTypedTableDataset,
                                  unittest.TestCase):

    def init_datastore(self):
        self._datastore = MockDatastore("mock_datastore")

    @property
    def datastore(self):
        return self._datastore

    def tweak_row_insert(self, dataset, rows):
        self.datastore.datasets_contents[dataset.name]["rows"] = rows
        return True

    def test_get_header(self):
        header = (('col_A', 'integer'),
                  ('col_B', 'varchar'),
                  ('col_C', 'boolean'),
                  ('col_D', 'float'))
        super(TestTypedTableMockDatastore, self).test_get_header(header)

    def test_get_header_exclude(self):
        header = (('col_A', 'integer'),
                  ('col_B', 'varchar'),
                  ('col_C', 'boolean'))
        name = 'table_dataset_header_exclude'
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        exclude_1 = ['col_A', ]
        header_1 = (('col_B', 'varchar'), ('col_C', 'boolean'))
        exclude_2 = ['col_C', 'col_B']
        header_2 = (('col_A', 'integer'),)
        self.assertEqual(dataset.get_header(exclude=exclude_1), header_1)
        self.assertEqual(dataset.get_header(exclude=exclude_2), header_2)

    def test_get_column_index(self):
        header = (('col_A', 'integer'),
                  ('col_B', 'varchar'),
                  ('col_C', 'boolean'))
        super(TestTypedTableMockDatastore, self).test_get_header(header)

    def test_get_rows(self):
        header = (('col_1', 'integer'),
                  ('col_2', 'varchar'))
        super(TestTypedTableMockDatastore, self).test_get_rows(header)

    def test_get_rows_exclude(self):
        header = (('col_1', 'integer'),
                  ('col_2', 'varchar'),
                  ('col_3', 'varchar'),)
        super(TestTypedTableMockDatastore, self).test_get_rows_exclude(header)

    def test_get_rows_filters(self):
        header = (('col_1', 'integer'),
                  ('col_2', 'varchar'))
        super(TestTypedTableMockDatastore, self).test_get_rows_filters(header)

    def test_export(self):
        header = (('col_1', 'integer'),
                  ('col_2', 'varchar'))
        super(TestTypedTableMockDatastore, self).test_export(header)


class TestTableCsvFolder(TestTableServiceTableDataset, unittest.TestCase):

    def init_datastore(self):
        self._datastore = CsvFolderDatastore(
            "csv_datastore",
            os.path.join(BASE_PATH, 'temp/csv_folder'),
            init=True,
        )

    @property
    def datastore(self):
        return self._datastore

    def tweak_row_insert(self, dataset, rows):
        filename = "{}.csv".format(dataset.name)
        header = dataset.get_header()
        self.datastore.write_csv_file(filename, rows, header=header)
        return True


class TestTableSQLDatastore(TestTableServiceTableDataset, unittest.TestCase):

    PG_PASSWORD = "postgres"
    HOST = "localhost"
    PG_URL = "postgresql://postgres:{}@{}/postgres".format(PG_PASSWORD, HOST)
    TEST_DATABASE_NAME = "pysoda_test"

    @classmethod
    def setUpClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("create database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    @classmethod
    def tearDownClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{}'
            AND pid <> pg_backend_pid();
        """.format(cls.TEST_DATABASE_NAME))
        conn.execute("commit")
        conn.execute("drop database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    def init_datastore(self):
        url = "postgresql://postgres:{}@{}/{}".format(
            self.PG_PASSWORD,
            self.HOST,
            self.TEST_DATABASE_NAME,
        )
        self._datastore = SQLDatastore("pg_sql_datastore", url, init=True)

    @property
    def datastore(self):
        return self._datastore

    def tweak_row_insert(self, dataset, rows):
        # TODO Optimize
        for row in rows:
            self.datastore.insert_row(dataset.name, row)
        return True
