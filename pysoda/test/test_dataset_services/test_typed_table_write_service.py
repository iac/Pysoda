# coding: utf-8

import abc
import unittest
import os

import sqlalchemy

from pysoda.src.datasets import *
from pysoda.src.datastores import *


BASE_PATH = os.path.dirname(__file__)


class TestTypedTableWService(abc.ABC):
    """
    Abstract base class testing typed table write service.
    A test subclass must be implemented for each dataset/datastore couple.
    """

    @abc.abstractmethod
    def init_datastore(self):
        pass

    @property
    @abc.abstractmethod
    def datastore(self):
        pass

    @abc.abstractmethod
    def make_dataset(self, name, header):
        pass

    def setUp(self):
        self.init_datastore()

    # Tests
    def test_append_row(self):
        name = 'tablew_dataset'
        header = (('col_A', 'integer'),
                  ('col_B', 'varchar'),
                  ('col_C', 'boolean'),
                  ('col_D', 'float'))
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        row = (1, 'baba', False, 4.2)
        dataset.append_row(row)
        self.assertEqual(dataset.get_rows(), [row, ])

    def test_append_rows(self):
        name = 'tablew_dataset_rows'
        header = (('col_1', 'integer'),
                  ('col_2', 'varchar'))
        self.datastore.register_dataset(self.make_dataset(name, header))
        dataset = self.datastore.get_dataset(name)
        rows = [(1, "Hello dude"),
                (2, "Bye bye man"),
                (3, "jobi-joba is my best friend")]
        dataset.append_rows(rows)
        self.assertEqual(dataset.get_rows(), rows)


class TestTypedTableWServiceTableDataset(TestTypedTableWService):
    """
    Base class for testing typed table write service with table datastore.
    """

    @abc.abstractmethod
    def init_datastore(self):
        pass

    @property
    @abc.abstractmethod
    def datastore(self):
        pass

    def make_dataset(self, name, header):
        return make_typed_table_write_dataset(name, header)


class TestTableWMockDatastore(TestTypedTableWServiceTableDataset,
                              unittest.TestCase):

    def init_datastore(self):
        self._datastore = MockDatastore("mock_datastore")

    @property
    def datastore(self):
        return self._datastore


class TestTableWCsvFolder(TestTypedTableWServiceTableDataset,
                          unittest.TestCase):

    def init_datastore(self):
        self._datastore = CsvFolderDatastore(
            "csv_datastore",
            os.path.join(BASE_PATH, 'temp/csv_folder'),
            init=True,
        )

    @property
    def datastore(self):
        return self._datastore


class TestTableWSQLDatastore(TestTypedTableWServiceTableDataset,
                             unittest.TestCase):

    PG_PASSWORD = "postgres"
    HOST = "localhost"
    PG_URL = "postgresql://postgres:{}@{}/postgres".format(PG_PASSWORD, HOST)
    TEST_DATABASE_NAME = "pysoda_test"

    @classmethod
    def setUpClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("create database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    @classmethod
    def tearDownClass(cls):
        engine = sqlalchemy.create_engine(cls.PG_URL)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{}'
            AND pid <> pg_backend_pid();
        """.format(cls.TEST_DATABASE_NAME))
        conn.execute("commit")
        conn.execute("drop database {}".format(cls.TEST_DATABASE_NAME))
        conn.close()

    def init_datastore(self):
        url = "postgresql://postgres:{}@{}/{}".format(
            self.PG_PASSWORD,
            self.HOST,
            self.TEST_DATABASE_NAME,
        )
        self._datastore = SQLDatastore("pg_sql_datastore", url, init=True)

    @property
    def datastore(self):
        return self._datastore
