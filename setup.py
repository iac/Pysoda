# coding: utf-8

from setuptools import setup, find_packages

setup(name='pysoda',
      version='0.1',
      description='service-oriented dataset abstraction library',
      url='https://gitlab.com/dimitri-justeau/Pysoda',
      author='Dimitri Justeau',
      author_email='dimitri.justeau@gmail.com',
      packages=find_packages(),
      install_requires=['sqlalchemy >= 1.0',
                        'geoalchemy2 >= 0.2',
                        'Shapely >= 1.5',
                        'django >= 1.8',
                        'geojson >= 1.3.1',
                        'numpy'])
